#include "flood-it.h"

void initialiser(int m[TAILLE][TAILLE])
{
	int i, j;
	for (i=0; i<TAILLE; i++)
	{
		for (j=0; j<TAILLE; j++)
		{
			m[i][j] = rand() % (COULEURS + 1);
		}
	}
}

void afficherGrille(int m[][TAILLE])
{
	printf("|");
	for (int j=0; j<TAILLE; j++){
		printf("---|");
	}
	printf("\n");
	for (int i=0; i<TAILLE; i++){
		
		printf("|");
		for (int j=0; j<TAILLE; j++){
			printf(" %d |", m[i][j]);
		}
		printf("\n");
		printf("|");
		for (int j=0; j<TAILLE; j++){
			printf("---|");
		}
		printf("\n");
	}
}

bool grilleUniforme(int m[][TAILLE]){
	int i, j;

	i = 0; 
	while (m[i][j] == m[0][0] && i != TAILLE-1)
	{
		j = 0;
		while (m[i][j] == m[0][0] && j != TAILLE-1)
		{
			j++;
		}
		i++;
	}

	return m[i][j] == m[0][0];
}


void remplirCase(int m[TAILLE][TAILLE], int couleurOriginale, int couleurNouvelle, int i, int j)
{
	if (((m[i][j] == couleurOriginale || m[i][j] == couleurNouvelle) && ((i!=0 && m[i-1][j] == couleurNouvelle) || (j!=0 && m[i][j-1] == couleurNouvelle))) || (i==0 && j==0))
	{
		m[i][j] = couleurNouvelle;
	}
}



void remplir(int m[][TAILLE], int couleurNouvelle)
{
	int couleurOriginale = m[0][0];
	for (int i=0; i<TAILLE; i++){
		for (int j=0; j<TAILLE; j++){
			remplirCase(m, couleurOriginale, couleurNouvelle, i,j);
		}
	}
}


int main()
{
	int m[TAILLE][TAILLE];
	int couleur;
	initialiser(m);
	afficherGrille(m);
	
	while (!grilleUniforme(m))
	{
		printf("Quelle couleur souhaitait vous transformer ? ");
		scanf("%d", &couleur);
		printf("\n");
		if (0<=couleur && couleur<=COULEURS){
			remplir(m, couleur);
			afficherGrille(m);
		}
	}
	return 0;
}
