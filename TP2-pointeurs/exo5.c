#include <stdio.h>
#include <stdlib.h>
#define N 1000000

int main(){
    int i;
    double *tab;
    tab = malloc(N*sizeof(double));

    for (i=0; i<N; i++){
        *(tab+i) = (double) i*i;
    }

    // for (i=0; i<N; i++){
    //     printf("%lf\n", *(tab+i));
    // }

    free(tab);
}