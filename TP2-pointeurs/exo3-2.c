#include <stdio.h>

int compter1(char * chaine) 
{   
  int i = 0;

  while (*(chaine+i) != '\0')
     ++i;

  return i; 
}

int compter2(char * chaine) 
{   
  char * s = chaine;

  while (*chaine != '\0')
     ++chaine;

  return chaine - s; 
}

int compter3(char * chaine) 
{   
  char * s = chaine;

  while (*chaine++);

  return chaine - s; 
}

int main(){
    printf("%d %d %d\n",compter1("SALUT"), compter2("SALUT"), compter3("SALUT")); 
}