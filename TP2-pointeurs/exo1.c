#include <stdio.h>


int main()
{
   int i  , *ptri  = &i;
   char c1 = '1', *ptrc1 = &c1;
   char c2 = '2', *ptrc2 = &c2;
   char *ptrc;
   double d1 = 1.0, *ptrd1 = &d1;


   printf(" ptri = %p  ptrc1 = %p  ptrd1=%p\n",ptri, ptrc1);
   printf("*ptri = %d *ptrc1 = %c *ptrd1=%lf\n", *ptri, *ptrc1, *ptrd1);
   printf("&ptri = %x &ptrc1 = %x &ptrd1=%x\n", &ptri, &ptrc1, &ptrd1);
   
   d1+=2;
   printf("Value incrementee : *ptrd1=%lf\n\n", *ptrd1);

   printf("Travail sur les caractères:\n");
   printf("c1 = %c c2 = %c\n\n", *ptrc1, *ptrc2);
   ptrc = ptrc1;
   ptrc1 = ptrc2;
   ptrc2 = ptrc;
   printf("c1 = %c c2 = %c\n\n", *ptrc1, *ptrc2);
   
   ptri++;
   ptrc1++;
   printf("ptri = %u ptrc1 = %d \n",ptri, ptrc1);
   // cela permet de voir la taille d'un int et d'un char en memoire
   // sizeof(int)  sizeof(char)

   return 0 ;
}
