#include <stdio.h>

void echangeParValeur(int a, int b){
    int buffer;
    printf("a=%d, b=%d\n", a, b);
    buffer = a;
    a = b;
    b = buffer;
    printf("a=%d, b=%d\n", a, b);
}

void echangeParAdresse(int *a, int *b){
    int buffer;
    buffer = *a;
    *a = *b;
    *b = buffer;
}

int main(){
    int i=0;
    int j=1;

    // echangeParValeur(i, j);
    printf("a=%d, b=%d\n", i, j);
    echangeParAdresse(&i, &j);
    printf("a=%d, b=%d\n", i, j);
}