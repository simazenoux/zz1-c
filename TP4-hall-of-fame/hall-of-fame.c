#include "hall-of-fame.h"


void afficherDonnees(FILE * file, Donnee_t d)
{
   fprintf(file, "%s : %s avec %d\n", d.nom, d.alias, d.score);
}


void saisirDonnees(FILE *file, Donnee_t * p)
{
   char buffer[100];
   
   fgets(p->nom, 100, file);
   fgets(p->alias, 40, file);

   fgets(buffer, 100, file);
   p->score = atoi(buffer);

   suppressionRetourChariot(p->nom);
   suppressionRetourChariot(p->alias);
}


char* suppressionRetourChariot(char chaine[]){
   if (chaine[strlen(chaine)-1] == '\n'){
      chaine[strlen(chaine)-1] = '\0';
   }

   return chaine;
}

int tableauFromFilename(char* nom_fichier, Donnee_t tableau[]){
   FILE * fichier;
   int taille = 0;

   fichier = fopen(nom_fichier, "r");
   if (fichier) 
   {
      while (!feof(fichier))
      {
         saisirDonnees(fichier, &tableau[taille]);
         taille++;
      }
   }
   fclose(fichier);
   
   return taille;
}


void afficherListe(Donnee_t * tete)
{
   Donnee_t * cour = tete;

   while (cour != NULL)
   {
      afficherDonnees(stdout, *cour);
      cour = cour->suivant;
   }
}

void afficherListeAvecIndex(Donnee_t * tete)
{
   Donnee_t * cour = tete;
   int i = 0;
   while (cour)
   {
      printf("%d : ", i);
      afficherDonnees(stdout, *cour);
      cour = cour->suivant;
   }
}


void insererListe(Donnee_t ** tete, Donnee_t * maillon)
{
   maillon->suivant = *tete;
   *tete = maillon;
}


void libererListe(Donnee_t * tete)
{
   Donnee_t ** pprec = &tete;
   Donnee_t * tmp;

   while (*pprec)
   {
      tmp = *pprec;
      pprec = (*pprec)->suivant;
      free(tmp);
   }
}



void afficherMenu(){
   printf("\n--------- MENU ---------\n\n");
   printf("1- Afficher la liste chainée\n");
   printf("2- Insérer (en tête) un nouvel élément\n");
   printf("3- Editer / modifier un élément\n");
   printf("4- Importer les données à partir d'un fichier texte\n");
   printf("5- Exporter les données actuelles vers un ficher\n");
   printf("6- Quitter\n");
}


int main(){
   char buffer[2]; 
   bool sortie = false;
   Donnee_t * tete = NULL;
   Donnee_t * maillon;

   while (!sortie){
      afficherMenu();
      fgets(buffer, 2, stdin);
      switch (atoi(buffer)){
         case 1:
            afficherListe(tete);
         break;

         case 2:
            maillon = malloc(sizeof(Donnee_t));
            char score[40];
            printf("Nom du jeu : ");
            fgets(maillon->nom, 100, stdin);
            printf("Alias : ");
            fgets(maillon->alias, 40, stdin);
            printf("Score : ");
            fgets(score, 40, stdin);
            maillon->score = atoi(score);
            suppressionRetourChariot(maillon->nom);
            suppressionRetourChariot(maillon->alias);

            insererListe(&tete, maillon);
         break;

         case 3:
            afficherListeAvecIndex(tete);
         break;

         case 4:
         break;

         case 5:
         break;

         case 6:
            libererListe(tete);
         break;
      }
   }
   
   return 0;
}