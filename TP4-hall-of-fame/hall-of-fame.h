#ifndef hall_of_fame_h
#define hall_of_fame_h
#define TAILLE_MAX 50

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* DECLARATION DES TYPES PERSONNELS */
// et de leur typedef si besoin

typedef struct Donnees{
    int score;
    char nom[100];
    char alias[40];
    struct Donnees * suivant;
} Donnees_t;


/* DECLARATIONS DES METHODES */
void afficherDonnees(FILE *, Donnees_t);
void saisirDonnees (FILE * , Donnees_t *);
char* suppressionRetourChariot(char chaine[]);
int tableauFromFilename(char* nom_fichier, Donnees_t tableau[]);
// mettre ici les autres declarations

#endif