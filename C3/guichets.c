#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NB 15



int guichet_A;
int guichet_B;
int guichet_C;
int guichet_D;
int guichet_E;
int guichet_F;
int guichet_G;
int guichet_H;
int guichet_I;
int guichet_J;
int guichet_K;
int guichet_L;
int guichet_M;
int guichet_N;
int guichet_O;



void initialiser(int *tab){
    int i;
    // Valeur aléatoire entre 0 et 2
    
    int random = rand() % NB;

    for (i=0; i< NB; i++){
        if (i == random){
            tab[i]= 1;
        }
        else{
            tab[i] = 0;
        }
    }
}

void afficherGuichetsOuverts(int *tab){
    int i;

    for (i=0; i<NB; i++){
        if (tab[i] == 1) {
            printf("guichet_%c est ouvert\n", i+'A');
        }
    }
}

int main(){
    int * tab[NB];
    tab[0] = &guichet_A;
    tab[1] = &guichet_B;
    tab[2] = &guichet_C;
    tab[3] = &guichet_D;
    tab[4] = &guichet_E;
    tab[5] = &guichet_F;
    tab[6] = &guichet_G;
    tab[7] = &guichet_H;
    tab[8] = &guichet_I;
    tab[9] = &guichet_J;
    tab[10] = &guichet_K;
    tab[11] = &guichet_L;
    tab[12] = &guichet_M;
    tab[13] = &guichet_N;
    tab[14] = &guichet_O;

    srand(time(0));
    initialiser(*tab);
    afficherGuichetsOuverts(*tab);

    return 0;
}