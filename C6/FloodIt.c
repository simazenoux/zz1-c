#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#define COULEURS 6
#define TAILLE 12
#define TRUE 1
#define FALSE 0

void initialiser(int m[][TAILLE]){
	for (int i=0; i<TAILLE; i++){
		for (int j=0; j<TAILLE; j++){
			m[i][j] = rand() % (COULEURS + 1);
		}
	}
}

void afficherGrille(int m[][TAILLE]){
	printf("|");
	for (int j=0; j<TAILLE; j++){
		printf("---|");
	}
	printf("\n");
	for (int i=0; i<TAILLE; i++){
		
		printf("|");
		for (int j=0; j<TAILLE; j++){
			printf(" %d |", m[i][j]);
		}
		printf("\n");
		printf("|");
		for (int j=0; j<TAILLE; j++){
			printf("---|");
		}
		printf("\n");
	}
}

int grilleUniforme(int m[][TAILLE]){
	int i = 0;
	int j= 0;
	int res = FALSE;
	while (m[i][j] == m[0][0] && i != TAILLE-1){
		j = 0;
		while (m[i][j] == m[0][0] && j != TAILLE-1){
			j++;
		}
		i++;
	}
	if (m[i][j] == m[0][0]){
		res = TRUE;
	}
	return res;
}


void remplirCase(int m[][TAILLE], int couleurOriginale, int couleurNouvelle, int i, int j){
	if (((m[i][j] == couleurOriginale || m[i][j] == couleurNouvelle) && ((i!=0 && m[i-1][j] == couleurNouvelle) || (j!=0 && m[i][j-1] == couleurNouvelle))) || (i==0 && j==0)){
		m[i][j] = couleurNouvelle;
	}
}



void remplir(int m[][TAILLE], int couleurNouvelle){
int couleurOriginale = m[0][0];
	for (int i=0; i<TAILLE; i++){
		for (int j=0; j<TAILLE; j++){
			remplirCase(m, couleurOriginale, couleurNouvelle, i,j);
		}
	}
}

void afficherEcran(SDL_Renderer *renderer, SDL_Rect rect){
	/* couleur de fond */
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
	SDL_RenderClear(renderer);

	/* dessiner en blanc */
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 0);
	rect.x = rect.y = 0;
	rect.w = rect.h = 600;
	SDL_RenderFillRect(renderer, &rect );

	/* afficher à l'ecran */
	SDL_RenderPresent(renderer);
}

int main(){
	int width = 800;
	int height = 700;
	int running = TRUE;
	
	if (SDL_Init(SDL_INIT_VIDEO) == -1){
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
		return EXIT_FAILURE; 
	}

	SDL_Window   * window;
	window = SDL_CreateWindow("SDL2 Programme 0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
				width, height, //largeur et hauteur 
				SDL_WINDOW_RESIZABLE); 
		
	if (window == 0) {
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
	}

	SDL_Renderer *renderer;
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
	if (renderer == 0) {
		fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
		return EXIT_FAILURE; 
	}

	SDL_Rect rect;
	afficherEcran(renderer, rect);

	//TODO : Virer a)rès la boucle normal
	SDL_Delay(5000);



	while (running) {
		while (SDL_PollEvent(&event))
		{
			switch(event.type)
			{
				case SDL_WINDOWEVENT:
					printf("window event\n");
					switch (event.window.event)  
					{
						case SDL_WINDOWEVENT_CLOSE:  
							printf("appui sur la croix\n");	
							break;
						case SDL_WINDOWEVENT_SIZE_CHANGED:
							width = event.window.data1;
							height = event.window.data2;
							printf("Size : %d%d\n", width, height);
						default:
							afficherEcran(renderer, rect);
					}   
					break;
				case SDL_MOUSEBUTTONDOWN:
					printf("Appui :%d %d\n", event.button.x, event.button.y);
					// afficherEcran() ?
					break;
				case SDL_QUIT : 
					printf("on quitte\n");    
					running = 0;
			}
		}	
		SDL_Delay(10); //  delai minimal
	}
	// int m[TAILLE][TAILLE];
	// int couleur;
	// initialiser(m);
	// afficherGrille(m);
	
	// while (!grilleUniforme(m)){
	// 	printf("Quelle couleur souhaitait vous transformer ? ");
	// 	scanf("%d", &couleur);
	// 	printf("\n");
	// 	if (0<=couleur && couleur<=COULEURS){
	// 		remplir(m, couleur);
	// 		afficherGrille(m);
	// 	}
	// }

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;
}
