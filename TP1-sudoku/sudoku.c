#include "sudoku.h"


int sommeNPremiersEntiers(int n)
{
    return n*(n+1)/2;
}


void afficher(int m[N][N])
{
	int i, j;

	printf(" ----------------------------- \n");
	for (j=0; j<N; j++) 
    {
		if (j%N == 0 && j!=0)
        {
			printf("|-----------------------------|\n");
		}
		for (i=0; i<N; i++)
        {
			if (i%N == 0)
				printf("|");
			if (m[i][j])
            {
				printf(" %d ", m[i][j]); 
			}
			else
            {
				printf("   ");
			}
		}
		printf("|\n");
	}
	printf(" ----------------------------- \n");
}


void initialiser(int m[N][N])
{
    int i,j;
    for(i=0; i<N; i++) 
    {
        for(j=0; j<N; j++)
        {
            m[i][j] = 0;
        }
    }
}


void generer(int m[N][N])
{
    int i, j, k;
    int n = sqrt(N);

    for(i=0; i<N; i++) 
    {
        for(j=0; j<N; j++)
        {
            m[i][j] = (i + j*n +j /n) %N +1 ; 
        }
    }
}


int decompter(int m[N][N])
{
    int i, j;
    int k = 0;

    for (i=0; i<N; i++) 
    {
        for (j=0; j<N; j++)
        {
            if (m[i][j] != 0)
            {
                k++;
            }

        }
    }
    return k;
}


bool verifierColonne(int m[N][N], int j)
{
    int i;
    int somme = 0;

    for (i=0; i<N; i++)
    {
        somme += m[i][j];
    }

    return somme-sommeNPremiersEntiers(N);
}


bool verifierLigne(int m[N][N], int i)
{
    int j;
    int somme = 0;

    for (j=0; j<N; j++)
    {
        somme += m[i][j];
    }

    return somme-sommeNPremiersEntiers(N);
}


bool verifierRegion(int m[N][N], int ir, int jr)
{
    int i, j;
    int n = sqrt(N);
    int somme = 0;

    for (i=ir*n; i < ir*(n+1); i++)
    {
        for (j=ir*n; j < jr*(n+1); j++)
        {
            somme += m[i][j];
        }
    }

    return somme-sommeNPremiersEntiers(N);
}


bool verifierGrille(int m[N][N])
{
    bool grilleCorrecte = true;
    int i, j;
    int n = sqrt(N);

    i=0;
    while (grilleCorrecte && i<=n)
    {
        j=0;
        while (grilleCorrecte && j<=n)
        {
            grilleCorrecte = verifierRegion(m, i, j);
        }
    }

    i=0;
    while (grilleCorrecte && i<=n)
    {
        grilleCorrecte = verifierLigne(m, i);
    }

    j=0;
    while (grilleCorrecte && j<=n)
    {
        grilleCorrecte = verifierColonne(m, j);
    }

    return grilleCorrecte;
}


bool saisir(int m[N][N], int remplissage)
{
    int code = 0;
    return code;
}


int main()
{
    int grille[N][N];
    int remplissage = 0;

    printf("SUDOKU\n");

    while (remplissage < N*N)
    {
        afficher(grille);
        saisir(grille, remplissage);
    }

    return 0;
}