#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste-chainee.h"


int main(int argc, char ** argv){

    liste_t liste;
    liste.tete = NULL;
    liste.fin = NULL;

    int i;
    
    FILE * file = NULL;
    
    if (argc-1){
        for (i=1; i<argc; i++){
            file = fopen(argv[i], "r");
            if (file != NULL) {
                importerListe(&liste, file);
                fclose(file);
            }
        }
    }
    else{
        saisirListe(&liste);
    }
    

    printf("\n\nLISTE\n");
    printf("Tete : %p\n", liste.tete);
    printf("Fin  : %p\n", liste.fin);

    printf("\n\nCONTENU :\n");
    afficherListe(&liste);

    libererListe(&liste);

    return 0;
}

