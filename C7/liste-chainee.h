#ifndef _LISTE_CHIANEE_
#define _LISTE_CHIANEE_

typedef struct cellule 
{
  char           * ligne;
  struct cellule * suiv;
} cellule_t;


typedef struct liste 
{
  cellule_t * tete;
  cellule_t * fin;
} liste_t;

void insererTete(liste_t * liste, cellule_t * cellule);
void insererFin(liste_t * liste, cellule_t * cellule);
void afficherListe(liste_t * liste);
void importerListe(liste_t * liste, FILE * steam);
void saisirListe(liste_t * liste);
void libererListe(liste_t * liste);

#endif