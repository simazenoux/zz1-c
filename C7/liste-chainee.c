#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "liste-chainee.h"

#define SIZE 2047

void insererTete(liste_t * liste, cellule_t * cellule){
    cellule->suiv = liste->tete;
    liste->tete = cellule;
    if (!liste->fin){
        liste->fin = cellule;
    }
}

void insererFin(liste_t * liste, cellule_t * cellule){
    if (liste->fin){
        liste->fin->suiv = cellule;
    }
    else{
        liste->tete = cellule;
    }
    liste->fin = cellule;
    cellule->suiv = NULL;
}


void importerListe(liste_t * liste, FILE * stream){
    cellule_t * cellule;
    char * ligne = (char *) malloc(SIZE * sizeof(char));

    while(fgets(ligne, 255, stream)){
        if (ligne[strlen(ligne) - 1] == '\n') 
            ligne[strlen(ligne) - 1] = '\0';
        cellule = (cellule_t *) malloc(sizeof(cellule));
        cellule->ligne = (char *) malloc(strlen(ligne) * sizeof(char));
        strcpy(cellule->ligne, ligne);
        insererFin(liste, cellule);
    }
    free(ligne);
}

void saisirListe(liste_t * liste){
    cellule_t * cellule;
    char * ligne = (char *) malloc(SIZE * sizeof(char));

    printf("Veuillez saisir votre ligne : ");
    while(fgets(ligne, 255, stdin)){
        if (ligne[strlen(ligne) - 1] == '\n') 
            ligne[strlen(ligne) - 1] = '\0';
        cellule = (cellule_t *) malloc(sizeof(cellule));
        cellule->ligne = (char *) malloc(strlen(ligne) * sizeof(char));
        strcpy(cellule->ligne, ligne);
        insererFin(liste, cellule);

        printf("Veuillez saisir votre ligne : ");
    }
    free(ligne);
}

void afficherListe(liste_t * liste){
    cellule_t * cour = liste->tete;
    while (cour){
        printf("%s : %p\n", cour->ligne, cour);
        cour = cour->suiv;
    }
    printf("\n\n");
}

void libererListe(liste_t * liste){
    cellule_t * cour = liste->tete;
    cellule_t * suiv;
    while (cour){
        suiv = cour->suiv;
        free(cour->ligne);
        free(cour);
        cour = suiv;
    }
}