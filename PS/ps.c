#include <stdio.h>
#include <stdlib.h>


int main(int argc, char ** argv){

    
    FILE * file = NULL;
    char * ligne;
    int i;
    int cpt;
    int code = 0;

    if (argc-1)
    {
        ligne = malloc(4096 * sizeof(char));
        if (ligne)
        {
            for (i=1; i<argc; i++)
            {
                file = fopen(argv[i], "r");
                if (file) {
                    cpt = 1;
                    while(fgets(ligne, 255, file)){
                        if (cpt == 4){
                            printf("PS 4 : Parce que <4\n");
                            cpt++;
                        }
                        printf("PS %d : %s", cpt++, ligne);
                    }
                    fclose(file);
                }
            }
            free(ligne);
        }
        else
        {
            code = 2;
        }
    }
    else
    {
        code = 1;
    }

    return 0;
}