#include <stdlib.h>
#include "fichiers-binaires.h"

void afficherTableau(Ligne_t * tab, int size)
{
    int i;
    for (i=0; i<size; i++)
    {
        printf("%s : écrit en %s en %d\n", tab[i].nom, tab[i].language, tab[i].annee);
    }
}

int main()
{

    int i;
    int taille = 20;
    ligne_t tableau[taille];

    for(i=0; i<taille; i++){
        tableau[i].nom = malloc(2*sizeof(char));
        tableau[i].language = malloc(2*sizeof(char));
        strcpy(tableau[i].nom, "a");
        strcpy(tableau[i].language, "a");
        tableau[i].annee = 1955 + i;
    }

    afficherTableau(tableau, taille);
    
    return 0;
}