#ifndef ARBRE_BINAIRE_H
#define ARBRE_BINAIRE_H

typedef struct Noeud
{
    int cle;
    struct Noeud * gauche;
    struct Noeud * droit;
} Noeud_t;

Noeud_t * initialiserNoeud(int cle);
void insererArbreBinaire(Noeud_t * arbre, Noeud_t noeud);
bool insererValeur(Noeud_t * arbre, int cle);


#endif