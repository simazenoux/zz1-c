#include <stdbool.h>
#include <stdio.h>
#include "arbre-binaire.h"



Noeud_t * initialiserNoeud(int cle)
{
    Noeud_t * nouv = NULL;
    nouv = malloc(sizeof(Noeud_t));
    if (nouv)
    {
        nouv->cle = cle;
        nouv->gauche = NULL;
        nouv->droit = NULL;
    }
    return nouv;
}


void insererArbreBinaire(Noeud_t * arbre, Noeud_t * noeud)
{
    while (arbre)
    {
        if (arbre->cle < noeud->cle)
        {
            arbre = arbre->gauche;
        }
        else
        {
            arbre = arbre->droit;
        }
    }
    arbre = noeud;
}


bool insererValeur(Noeud_t * arbre, int cle)
{
    Noeud_t * nouv = initialiserNoeud(cle);
    if (nouv)
    {
        insererArbreBinaire(arbre, nouv);
    }
    return nouv;
}


Noeud_t ** rechercherValeur(Noeud_t * arbre, int cle)
{
    Noeud_t * nouv = initialiserNoeud(cle);
    if (nouv)
    {
        insererArbreBinaire(arbre, nouv);
    }
    return nouv;
}