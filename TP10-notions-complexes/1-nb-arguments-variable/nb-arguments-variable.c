#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdarg.h>

void fonction1(int nbVariables, ... ){
    va_list args;
    va_start(args, nbVariables);
    while (nbVariables--)
    {
        printf("%c",va_arg(args, char *));
    }
    va_end(args);
}


int main()
{
    struct dirent * lecture;
	DIR *rep;
	rep = opendir(".");
	while (lecture = readdir(rep))
    {
	    printf("%s\n", lecture->d_name);
	}
    fonction1(2, "tp1", "tp2");
	closedir(rep);
}