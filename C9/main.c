#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char * OPER_NAMES[] = { "x", "sin(x)", "cos(x)", "log(x)", "exp(x)", NULL };

typedef enum ope {
    NONE = -1, ID , SIN, COS, LOG, EXP
} OP;

int nombreOperateurs = EXP - NONE + 1;

/*
int identification(const char * operateur){
    int i = 0;
    while (OPER_NAMES[i] && strcmp(OPER_NAMES[i], operateur)){
        i++;
    }
    if (!OPER_NAMES[i]){
        i = -1;
    }
    return i;
}*/

OP identification(const char * commande){
    OP op= EXP;

    while (OPER_NAMES[op] && strcmp(OPER_NAMES[op], commande)){
        op--;
    }

    return op;
}



double identite(double __x){
    return __x;
}

double erreur(double __x){
    fprintf(stderr, "ERREUR\n");
    return __x;
}

double (*OPER_FN [])(double) = { identite, sin, cos, log, exp, erreur };

double evalp(double v, OP operateur){
    double res;
    if (operateur == -1){
        res = (*OPER_FN[5])(v);
    }
    else{
        res = (*OPER_FN[operateur])(v);
    }

    return res;
}

double evalf(double v, OP operateur){
    double res;
    switch (operateur)
    {
    case 0:
        res = v;
        break;

    case 1:
        res = sin(v);
        break;
    
    case 2:
        res = cos(v);
        break;

    case 3:
        res = log(v);
        break;
    
    case 4:
        res = exp(v);
        break;
    }
    return res;
}

void calcul(double a, double b, double delta, OP op, FILE * fichier){
    double i;
    for (i=a; i<=b; i+=delta){
        fprintf(fichier, "%f ", evalp(i, op));
    }
}

void calculADeuxParametres(double a, double b, double delta, OP exp1, OP exp2, FILE * fichier){
    double i;
    for (i=a; i<=b; i+=delta){
        fprintf(fichier, "%f ", evalp(i, exp1), evalp(i,exp2));
    }
}

void calculatrice(){
    int i;
    OP op;
    double a, b, delta;
    char * endPtr;
    char * commande = (char *) malloc(255*sizeof(char));

    do{
        i=0;
        printf("--- OPERATEURS ---\n");
        while (OPER_NAMES[i]){
            printf("%s\n",OPER_NAMES[i]);
            i++;
        }
        printf("\n\nSaisir votre opérateur :");
        fgets(commande, 255, stdin);
        i=0;
        while (commande[i]!= '\n' && commande[i]!= '\0'){
            i++;
        }
        commande[i] = '\0';

        op = identification(commande);

    } while (op == NONE);

    printf("Saisir la borne inférieure 'a' : ");
    fgets(commande, 255, stdin);
    a = strtod(commande, &endPtr);

    do{
        printf("Saisir la borne suppérieure 'b' : ");
        fgets(commande, 255, stdin);
        b = strtod(commande, &endPtr);
    } while (a > b);
    
    do{
        printf("Saisir le pas 'delta' : ");
        fgets(commande, 255, stdin);
        delta = strtod(commande, &endPtr);
    } while (delta <= 0);
    

    calcul(a,b,delta,op, stdout);
}

void calculatriceADeuxParametres(){
    int i;
    char
    OP exp1, exp2;
    char op
    double a, b, delta;
    char * endPtr;
    char * commande = (char *) malloc(255*sizeof(char));

    do{
        i=0;
        printf("--- OPERATEURS ---\n");
        while (OPER_NAMES[i]){
            printf("%s\n",OPER_NAMES[i]);
            i++;
        }
        printf("\n\nSaisir votre opération :");
        fgets(commande, 255, stdin);
        sscanf (commande, "%s %c %s", exp1, &op, exp2);
        i=0;
        while (commande[i]!= '\n' && commande[i]!= '\0'){
            i++;
        }
        commande[i] = '\0';

        op = identification(commande);

    } while (op == NONE);

    printf("Saisir la borne inférieure 'a' : ");
    fgets(commande, 255, stdin);
    a = strtod(commande, &endPtr);

    do{
        printf("Saisir la borne suppérieure 'b' : ");
        fgets(commande, 255, stdin);
        b = strtod(commande, &endPtr);
    } while (a > b);
    
    do{
        printf("Saisir le pas 'delta' : ");
        fgets(commande, 255, stdin);
        delta = strtod(commande, &endPtr);
    } while (delta <= 0);
    

    calcul(a,b,delta,op, stdout);
}

int main(){
    FILE * fichier;
    fichier = fopen("resultats.txt", "w");

    printf("%f\n\n", evalp(1, NONE));
    calcul(1.5, 2.5, 0.51, COS, fichier);

    calculatrice();

    fclose(fichier);
    return 0;
}