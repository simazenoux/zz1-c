#ifndef __COMMUN__
#define __COMMUN__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "liste.h"
#include "histo.h"

#define ERROR_OK          0
#define ERROR_LIST_ALLOC  1
#define ERROR_FILE        1
#define DEBUG             1

#ifdef DEBUG
    #define LOG(A) printf A
#else
    #define LOG(A) 
#endif

int ERROR = 0;

#endif